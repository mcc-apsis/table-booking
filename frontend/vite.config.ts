import { defineConfig, loadEnv } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  const env = loadEnv(mode, process.cwd(), "");
  return {
    plugins: [vue()],
    base: env.VITE_TBS_BASE,
    rollupOptions: {
      // overwrite default .html entry
      input: "src/main.ts",
    },
    build: {
      manifest: true,
      sourcemap: true,
    },
    parserOptions: {
      optionalChainingAssign: true,
    },
  };
});
