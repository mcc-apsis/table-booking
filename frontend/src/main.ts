import { createApp, watch } from "vue";
import { createRouter, createWebHashHistory } from "vue-router";
import { createPinia } from "pinia";

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap";
import "bootstrap-icons/font/bootstrap-icons.min.css";

import "./style.scss";

import App from "./App.vue";

import DesksView from "./views/DesksView.vue";
import LandingView from "./views/LandingView.vue";
import PeopleView from "./views/PeopleView.vue";
import MeetingsView from "./views/MeetingsView.vue";

import { useContextStore } from "./data/store.ts";
import GroupView from "./views/GroupView.vue";

const pinia = createPinia();

watch(
  pinia.state,
  (state) => {
    // persist the whole state to the local storage whenever it changes
    localStorage.setItem(
      "mcc-desk-booking-context",
      JSON.stringify(state.context),
    );
  },
  { deep: true },
);

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    { name: "landing", path: "/:user?", component: LandingView },
    { name: "desks", path: "/desks/:user?", component: DesksView },
    { name: "people", path: "/people/:user?", component: PeopleView },
    { name: "meetings", path: "/meetings/:user?", component: MeetingsView },
    { name: "group", path: "/group/:user?/g/:group?", component: GroupView },
  ],
});

createApp(App).use(router).use(pinia).mount("#app");

router.beforeEach(async (to) => {
  const contextStore = useContextStore();

  if (
    to.params?.user &&
    /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/.test(
      to.params.user as string,
    )
  ) {
    contextStore.user = to.params.user as string;
  }

  if (!contextStore.user && to.name !== "landing") {
    return { name: "landing" };
  }

  if (to.name === "landing" && contextStore.user) {
    return { name: "desks" };
  }

  return true;
});
