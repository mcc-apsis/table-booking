import type { Desk, DeskLookup, Room } from "./types";

export const meetingRoomsGrouped: Record<string, Desk[]> = {
  Upstairs: [
    { id: 100, name: "Conference Room", info: "Capacity: 30 people" },
    { id: 101, name: "Library", info: "Capacity: 8 people" },
    // { id: 102, name: 'Ottmar\'s office', info: 'Capacity: 6 people' },
  ],
  Downstairs: [{ id: 103, name: "Seminar Room", info: "Capacity: 6 people" }],

  // "Zoom rooms": [
  //   { id: 201, name: "APSIS zoom", info: "Zoom room (managed by APSIS group)" },
  //   { id: 202, name: "MCC zoom 1", info: "Zoom room (managed by MCC)" },
  //   { id: 203, name: "MCC zoom 2", info: "Zoom room (managed by MCC)" },
  // ],
};

export const meetingRooms = Object.values(meetingRoomsGrouped).flatMap(
  (entry) => entry,
);
export const meetingRoomLookup = Object.fromEntries(
  meetingRooms.map((room) => [room.id, room]),
);

export const desks: Room[] = [
  {
    name: "1.01",
    desc: "Arrhenius Room",
    desks: [
      { id: 61, name: "T1", owner: "??" },
      { id: 62, name: "T2", owner: "??" },
      { id: 63, name: "T3", owner: "??" },
      { id: 64, name: "T4", owner: "??" },
      { id: 65, name: "T5", owner: "??" },
    ],
  },
  {
    name: "1.02",
    desc: "Policy Unit",
    desks: [
      { id: 13, name: "T1", owner: "Celine" },
      { id: 14, name: "T2", owner: "Maria" },
      { id: 15, name: "T3", owner: "Katja" },
      { id: 16, name: "T4", owner: "Marianna" },
      { id: 43, name: "T5", owner: "??" },
      { id: 44, name: "T6", owner: "??" },
    ],
  },
  {
    name: "1.04",
    desc: "Tyndall Room",
    desks: [
      { id: 17, name: "T1", owner: "??" },
      { id: 18, name: "T2", owner: "??" },
      { id: 19, name: "T3", owner: "??" },
    ],
  },
  {
    name: "1.06",
    desc: "Flex Office",
    desks: [
      { id: 20, name: "T1", owner: "Johannes" },
      { id: 21, name: "T2", owner: "Leonard" },
      { id: 22, name: "T3", owner: "Flex desk", flex: true },
      { id: 23, name: "T4", owner: "Flex desk", flex: true },
      { id: 24, name: "T5", owner: "Flex desk", flex: true },
      { id: 25, name: "T6", owner: "Flex desk", flex: true },
    ],
  },
  {
    name: "1.14",
    desc: "Schellnhuber Room",
    desks: [
      { id: 26, name: "T1", owner: "??" },
      { id: 27, name: "T2", owner: "??" },
      { id: 28, name: "T3", owner: "??" },
    ],
  },
  // {
  //   name: '1.15',
  //   desc: 'Jan',
  //   desks: [
  //     { id: 9, name: 'T1', owner: 'Jan' },
  //   ]
  // },
  {
    name: "1.16",
    desc: "Machine Room",
    desks: [
      { id: 5, name: "T1", owner: "Will" },
      { id: 6, name: "T2", owner: "Sarah" },
      { id: 7, name: "T3", owner: "Flex desk" },
      { id: 8, name: "T4", owner: "Finn" },
    ],
  },
  {
    name: "1.17",
    desc: "Engine Room",
    desks: [
      { id: 1, name: "T1", owner: "Diana" },
      { id: 2, name: "T2", owner: "Niklas" },
      { id: 3, name: "T3", owner: "Tim" },
      {
        id: 4,
        name: "T4",
        owner: "Klaas",
        info: "Consider free when not occupied by 10:30am",
      },
    ],
  },
  {
    name: "0.01",
    desc: "Skea Room",
    desks: [
      { id: 29, name: "T1", owner: "??" },
      { id: 30, name: "T2", owner: "??" },
      { id: 31, name: "T3", owner: "??" },
    ],
  },
  {
    name: "0.08",
    desc: "Thunberg Room",
    desks: [
      { id: 32, name: "T1", owner: "??" },
      { id: 33, name: "T2", owner: "??" },
      { id: 34, name: "T3", owner: "??" },
    ],
  },
  {
    name: "0.10",
    desc: "Hansen Room",
    desks: [
      { id: 35, name: "T1", owner: "??" },
      { id: 36, name: "T2", owner: "??" },
      { id: 37, name: "T3", owner: "??" },
    ],
  },
  {
    name: "0.13",
    desc: "Pigou Room",
    desks: [
      { id: 38, name: "T1", owner: "??" },
      { id: 39, name: "T2", owner: "??" },
    ],
  },
  {
    name: "0.14",
    desc: "Rosenzweig Room",
    desks: [
      { id: 40, name: "T1", owner: "??" },
      { id: 41, name: "T2", owner: "??" },
      { id: 42, name: "T3", owner: "??" },
    ],
  },
  {
    name: "The Tower",
    desc: "The Tower",
    desks: [
      { id: 50, name: "T1", owner: "Leonard" },
      { id: 51, name: "T2", owner: "Flex desk", flex: true },
      { id: 52, name: "T3", owner: "Flex desk", flex: true },
      { id: 53, name: "T4", owner: "Sreeja" },
      { id: 54, name: "T5", owner: "Xiaoran" },
      { id: 55, name: "T6", owner: "Farah" },
      { id: 56, name: "T7", owner: "Johannes" },
      { id: 57, name: "T8", owner: "Ganesh" },
      { id: 58, name: "T9", owner: "Tommy" },
      { id: 59, name: "T10", owner: "Tessa" },
    ],
  },
  {
    name: "A31–0.08",
    desc: "Only for CERES, Policy Unit, or Students of the directors; Keys in office 0.05 or 1.20",
    desks: [
      { id: 71, name: "T1", owner: "Flex desk", flex: true },
      { id: 72, name: "T2", owner: "Flex desk", flex: true },
      { id: 73, name: "T3", owner: "Flex desk", flex: true },
      { id: 74, name: "T4", owner: "Flex desk", flex: true },
    ],
  },
];

export const deskLookup: Record<number, DeskLookup> = Object.fromEntries(
  desks.flatMap((room: Room, ri: number) =>
    room.desks.map((desk: Desk) => [
      desk.id,
      {
        ...desk,
        roomIdx: ri,
        roomName: room.name,
        roomDesc: room.desc,
      } as DeskLookup,
    ]),
  ),
);
