export type BookingType = "FULL_DAY" | "HALF_DAY" | "FREE" | "MEETING";
export type BookingMode = BookingType | "CLEAR";
export type PersonType = "STUD" | "FULL" | "GUEST";
export type ToastType = "SUCC" | "FAIL" | "WARN" | "INFO";

export interface Toast {
  idx: number;
  msg: string;
  typ: ToastType;
}

export interface Holiday {
  date: Date;
  name: string;
}

export interface Booking {
  id: string;
  day: Date;
  desk: number;
  person: string;
  type: BookingType;

  series?: string;
  name?: string;
  note?: string;
  begin?: Date;
  end?: Date;
}

export interface DeskLookup {
  id: number;
  name: string;
  desc?: string;
  main?: string; // Person.id
  roomIdx: number;
  roomName: string;
  roomDesc?: string;
}

export interface Desk {
  id: number;
  name: string;
  owner?: string;
  info?: string;
  main?: string; // Person.id
  flex?: boolean;
}

export interface Room {
  name: string;
  desc?: string;
  desks: Desk[];
}

export interface Person {
  id: string;
  name: string;
  short: string;
  acro: string;
  group?: string;
  type: PersonType;
}

export interface GroupingAPI {
  id: string;
  name: string;
  owner: string;
  public: boolean;
  people: string;
}

export interface Grouping extends GroupingAPI {
  people: string[];
}

export type Day = { day: Date } & Record<number, Booking[]>; // key is desk id
