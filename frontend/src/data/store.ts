import { defineStore } from "pinia";
import { computed, Ref, ref, watch } from "vue";
import { useRoute, useRouter } from "vue-router";
import { desks } from "./rooms.ts";
import { GET } from "./api.ts";
import type { Grouping, GroupingAPI, Person, Toast, ToastType } from "./types";

export const useContextStore = defineStore("context", () => {
  const router = useRouter();

  const browserStore = localStorage.getItem("mcc-desk-booking-context");
  const stored = !browserStore ? {} : JSON.parse(browserStore);

  const user = ref<string | null>(stored.user ?? null);

  const showWeekends = ref<boolean>(stored.showWeekends ?? false);
  const showPre7am = ref<boolean>(stored.showPre7am ?? false);
  const selectedRooms = ref<string[]>(stored.selectedRooms ?? []);
  const selectedMeetingRooms = ref<number[]>(
    stored.selectedMeetingRooms ?? [100, 101, 102],
  );

  // initially, select all rooms
  if (selectedRooms.value.length === 0) {
    selectedRooms.value = desks.map((room) => room.name);
  }

  const hasSecret = computed(() => !!user.value);

  function pickUser(uid: string) {
    router.push({ params: { user: uid } });
    user.value = uid;
  }

  function logOut() {
    user.value = null;
    router.push({ name: "landing" });
  }

  return {
    user,
    showWeekends,
    showPre7am,
    selectedRooms,
    selectedMeetingRooms,
    hasSecret,
    logOut,
    pickUser,
    isSafari: /^((?!chrome|android).)*safari/i.test(navigator.userAgent),
    // @ts-ignore
    isMobile: !!navigator.userAgentData?.mobile,
  };
});

export const useToastStore = defineStore("toasts", () => {
  const toasts: Ref<Toast[]> = ref<Toast[]>([]);
  let toastCount = 0;

  function toast(msg: string, typ: ToastType = "INFO") {
    const tst = { idx: toastCount, msg, typ };
    toasts.value.push(tst);
    setTimeout(() => clearToast(tst.idx), 10000);
    toastCount += 1;
  }

  function clearToast(idx: number) {
    const index = toasts.value.map((toast: Toast) => toast.idx).indexOf(idx);
    if (index >= 0) {
      toasts.value.splice(index, 1);
    }
  }

  return { clearToast, toast, toasts };
});

export const usePeopleStore = defineStore("people", () => {
  // const lastLoaded = Date.now();

  const toaster = useToastStore();
  const people = ref<Record<string, Person> | null>(null);
  const sortedPeople = computed(() => {
    if (!people.value) return [];
    const peeps = Object.values(people.value);
    peeps.sort((p1, p2) => {
      const n1 = p1.name.toLowerCase();
      const n2 = p2.name.toLowerCase();
      if (n1 < n2) return -1;
      if (n1 > n2) return 1;
      return 0;
    });
    return peeps;
  });

  function reload() {
    GET("/people/")
      .then((response) => {
        people.value = Object.fromEntries(
          (response as Person[]).map((person: Person) => [person.id, person]),
        );
      })
      .catch(() => {
        toaster.toast("Failed to load people", "FAIL");
      });
  }

  reload();
  return { reload, sortedPeople, people };
});

export const useGroupingStore = defineStore("groupings", () => {
  const router = useRouter();
  const route = useRoute();

  const toaster = useToastStore();

  const activeGroup = ref<string>(
    localStorage.getItem("mcc-desk-booking-group") || "",
  );

  watch(activeGroup, (value: string) => {
    localStorage.setItem("mcc-desk-booking-group", value);
    router.push({ name: "group", params: { group: value } });
  });

  if (route.params.group) {
    activeGroup.value = route.params.group as string;
  }

  const groups = ref<Record<string, Grouping>>({});
  const group = computed<Grouping | null>(
    () => groups.value[activeGroup.value] ?? null,
  );

  function reload() {
    GET("/groupings/")
      .then((response) => {
        // try to pre-select by picking the first one if exists
        if (activeGroup.value.length === 0) {
          //@ts-ignore
          activeGroup.value = response[0]?.id ?? "";
        }

        // parse response and populate store
        groups.value = Object.fromEntries(
          (response as GroupingAPI[]).map((grp: GroupingAPI) => [
            grp.id,
            {
              ...grp,
              people: JSON.parse(grp.people),
            },
          ]),
        );
      })
      .catch(() => {
        toaster.toast("Failed to load people", "FAIL");
      });
  }

  reload();
  return { reload, group, groups, activeGroup };
});

export const useApiStore = defineStore("api", () => {
  const requests = ref<number[]>([]);
  const counter = ref<number>(0);
  const isOutdated = ref<boolean>(false);
  let timeout: number | undefined = undefined;

  function startRequest(): number {
    requests.value.push(counter.value);
    counter.value += 1;

    clearTimeout(timeout);
    timeout = undefined;

    return counter.value;
  }

  function finishRequest(cnt: number) {
    const index = requests.value.indexOf(cnt);
    if (cnt >= 0) {
      requests.value.splice(index, 1);
    }
    timeout = setTimeout(() => {
      isOutdated.value = true;
    }, 600000); // outdated after 10 min
  }

  const isLoading = computed(() => requests.value.length > 0);

  return { startRequest, finishRequest, isLoading, isOutdated };
});

// export const contextStore = useContextStore();
