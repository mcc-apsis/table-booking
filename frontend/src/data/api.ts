import { useApiStore, useContextStore } from "./store.ts";
import { Booking } from "./types";

const API_BASE = import.meta.env.VITE_TBS_BACKEND;

export function d2s(d: Date) {
  return `${d.getFullYear()}-${("0000" + (d.getMonth() + 1)).slice(-2)}-${("0000" + d.getDate()).slice(-2)}`;
  //return d.toISOString().split('T')[0]; // nicer, but timezone issues
}

export function ts2s(d: Date) {
  return `${d2s(d)}T${("0000" + d.getHours()).slice(-2)}:${("0000" + d.getMinutes()).slice(-2)}:${("0000" + d.getSeconds()).slice(-2)}`;
}

export function preparePayload(b: Booking) {
  return {
    ...b,
    day: d2s(b.day),
    begin: b.begin ? ts2s(b.begin) : undefined,
    end: b.end ? ts2s(b.end) : undefined,
  };
}

export function cloneBooking(b: Booking): Booking {
  const clone = JSON.parse(JSON.stringify(preparePayload(b)));
  if (clone.day) clone.day = new Date(clone.day);
  if (clone.begin) clone.begin = new Date(clone.begin);
  if (clone.end) clone.end = new Date(clone.end);
  return clone;
}

function parseResponse(response: string) {
  try {
    return JSON.parse(response);
  } catch {
    return response;
  }
}

export function GET<T>(
  path: string,
  params: Record<string, string> | null = null,
): Promise<T> {
  const apiStore = useApiStore();
  const contextStore = useContextStore();
  const req = apiStore.startRequest();
  let url = API_BASE + path;
  if (params) {
    url +=
      "?" +
      Object.entries(params)
        .map((entry) => `${entry[0]}=${encodeURIComponent(entry[1])}`)
        .join("&");
  }

  return new Promise(function (resolve, reject) {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (this.readyState === 4) {
        apiStore.finishRequest(req);
        if (this.status === 200) {
          const result = parseResponse(this.responseText);
          resolve(result);
        } else {
          reject("Error " + this.status + " " + this.statusText);
        }
      }
    };

    xhr.open("GET", url, true);
    xhr.setRequestHeader("X-UID", contextStore.user as string);
    xhr.send();
  });
}

export function DELETE<T>(
  path: string,
  params: Record<string, string> | null = null,
): Promise<T> {
  const apiStore = useApiStore();
  const contextStore = useContextStore();
  const req = apiStore.startRequest();
  let url = API_BASE + path;
  if (params) {
    url +=
      "?" +
      Object.entries(params)
        .map((entry) => `${entry[0]}=${encodeURIComponent(entry[1])}`)
        .join("&");
  }

  return new Promise(function (resolve, reject) {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (this.readyState === 4) {
        apiStore.finishRequest(req);
        if (this.status === 200) {
          const result = parseResponse(this.responseText);
          resolve(result);
        } else {
          reject("Error " + this.status + " " + this.statusText);
        }
      }
    };

    xhr.open("DELETE", url, true);
    xhr.setRequestHeader("X-UID", contextStore.user as string);
    xhr.send();
  });
}

export function POST<T>(
  path: string,
  payload: Record<string, any>,
): Promise<T> {
  const apiStore = useApiStore();
  const contextStore = useContextStore();
  const req = apiStore.startRequest();

  return new Promise(function (resolve, reject) {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (this.readyState === 4) {
        apiStore.finishRequest(req);
        if (this.status === 200) {
          const result = parseResponse(this.responseText);
          resolve(result);
        } else {
          reject("Error " + this.status + " " + this.statusText);
        }
      }
    };

    // send list of elements as JSON to the server
    xhr.open("POST", API_BASE + path, true);
    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    xhr.setRequestHeader("X-UID", contextStore.user as string);
    xhr.send(JSON.stringify(payload));
  });
}
