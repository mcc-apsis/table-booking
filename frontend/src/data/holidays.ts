import type { Holiday } from "./types";

// remember, months start at 0, dates at 1
export const holidays: Record<string, Holiday> = Object.fromEntries(
  [
    // 2024
    { date: new Date(2024, 2, 8), name: "International Women's day" },
    { date: new Date(2024, 2, 29), name: "Good Friday" },
    { date: new Date(2024, 3, 1), name: "Easter Monday" },
    { date: new Date(2024, 4, 1), name: "1. Mai" },
    { date: new Date(2024, 4, 9), name: "Himmelfahrt" },
    { date: new Date(2024, 4, 20), name: "Pfingstmontag" },
    { date: new Date(2024, 9, 3), name: "Tag der Deutschen Einheit" },
    { date: new Date(2024, 11, 24), name: "Christmas" },
    { date: new Date(2024, 11, 25), name: "Christmas" },
    { date: new Date(2024, 11, 26), name: "Christmas" },

    // 2025
    { date: new Date(2025, 0, 1), name: "Neujahr" },
    { date: new Date(2025, 2, 8), name: "International Women's day" },
    { date: new Date(2025, 3, 18), name: "Good Friday" },
    { date: new Date(2025, 3, 21), name: "Easter Monday" },
    { date: new Date(2025, 4, 1), name: "1. Mai" },
    { date: new Date(2025, 4, 29), name: "Himmelfahrt" },
    { date: new Date(2025, 5, 9), name: "Pfingstmontag" },
    { date: new Date(2025, 9, 3), name: "Tag der Deutschen Einheit" },
    { date: new Date(2025, 11, 24), name: "Christmas" },
    { date: new Date(2025, 11, 25), name: "Christmas" },
    { date: new Date(2025, 11, 26), name: "Christmas" },
  ]
    // transform it into an object so we can look up dates more easily
    .map((holiday: Holiday) => [holiday.date.toDateString(), holiday]),
);
