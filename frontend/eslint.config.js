import path from "node:path";
import { fileURLToPath } from "node:url";
import js from "@eslint/js";
import { FlatCompat } from "@eslint/eslintrc";
import vueTsEslintConfig from "@vue/eslint-config-typescript";
import prettierConfig from "@vue/eslint-config-prettier";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
  baseDirectory: __dirname,
  recommendedConfig: js.configs.recommended,
  allConfig: js.configs.all,
});
export default [
  { ignores: [".gitignore", "tools/", "dist/"] },
  // ...pluginVue.configs["flat/recommended"],
  ...compat.extends("plugin:vue/vue3-essential"),
  ...vueTsEslintConfig({
    extends: ["recommended"],
    rootDir: import.meta.dirname,
  }),
  prettierConfig,
  {
    languageOptions: {
      ecmaVersion: "latest",
      sourceType: "script",
    },
    rules: {
      // override/add rules settings here, such as:
      // 'vue/no-unused-vars': 'error'
      "@typescript-eslint/ban-ts-comment": "off",
      "vue/require-v-for-key": "off",
      "vue/valid-v-for": "off",
      "vue/html-self-closing": "off",
      "@typescript-eslint/no-explicit-any": "off",
      "vue/no-v-html": "off",

      "vue/max-len": [
        "error",
        {
          code: 140,
          ignorePattern: '(d|style)="[^"]*"',
          ignoreStrings: true,
          ignoreTrailingComments: true,
        },
      ],

      "object-curly-newline": "off",
      "class-methods-use-this": "off",
      "prefer-promise-reject-errors": "off",
      "vuejs-accessibility/click-events-have-key-events": "off",
      "vue/no-mutating-props": "off",

      "operator-linebreak": [
        "error",
        "after",
        {
          overrides: {
            "?": "before",
            ":": "before",
          },
        },
      ],

      "consistent-return": [
        "error",
        {
          treatUndefinedAsUnspecified: false,
        },
      ],
    },
  },
  {
    files: [
      "**/*.vue",
      "**/*.js",
      "**/*.jsx",
      "**/*.cjs",
      "**/*.mjs",
      "**/*.ts",
      "**/*.tsx",
      "**/*.cts",
      "**/*.mts",
    ],
    ignores: [".gitignore", "**/tools/*", "**/dist/*"],
  },
];
