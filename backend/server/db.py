from enum import Enum

from sqlalchemy import create_engine, Column, String, Integer, DateTime, Date, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from pydantic import BaseModel
from datetime import date, datetime

from .config import settings

SQLALCHEMY_DATABASE_URL = f'sqlite:///{settings.DB_FILE}'

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={'check_same_thread': False}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


class Person(Base):
    __tablename__ = 'people'

    id = Column(String, primary_key=True)
    name = Column(String)
    short = Column(String)
    acro = Column(String)
    group = Column(String)
    type = Column(String)


class Booking(Base):
    __tablename__ = 'bookings'
    # __table_args__ = (
    #     UniqueConstraint('day', 'desk','person'),
    # )

    id = Column(String, primary_key=True)
    day = Column(Date)
    desk = Column(Integer)
    person = Column(String)
    type = Column(String)

    series = Column(String, nullable=True)
    name = Column(String, nullable=True)
    note = Column(String, nullable=True)
    begin = Column(DateTime, nullable=True)
    end = Column(DateTime, nullable=True)


class Grouping(Base):
    __tablename__ = 'groupings'

    id = Column(String, primary_key=True)
    owner = Column(String)
    name = Column(String)
    public = Column(Boolean)  # if TRUE, visible to all
    people = Column(String)  # JSON array of strings (`Person.id`)


class BookingType(str, Enum):
    MEETING = 'MEETING'
    FULL_DAY = 'FULL_DAY'
    HALF_DAY = 'HALF_DAY'
    FREE = 'FREE'


class PersonType(str, Enum):
    STUD = 'STUD'
    FULL = 'FULL'
    GUEST = 'GUEST'


class PersonModel(BaseModel):
    id: str
    name: str
    short: str
    acro: str
    group: str
    type: PersonType

    class Config:
        from_attributes = True


class BookingModel(BaseModel):
    id: str
    day: date
    desk: int
    person: str
    type: BookingType
    series: str | None = None
    name: str | None = None
    note: str | None = None
    begin: datetime | None = None
    end: datetime | None = None

    class Config:
        from_attributes = True


class GroupingModel(BaseModel):
    id: str
    owner: str
    name: str
    public: bool
    people: str

    class Config:
        from_attributes = True
