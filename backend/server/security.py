from fastapi import status as http_status, Header

from .db import SessionLocal, Person, PersonModel


class InsufficientPermissions(Exception):
    status = http_status.HTTP_403_FORBIDDEN
    headers = {'WWW-Authenticate': 'Bearer'}


class NotAuthenticated(Exception):
    status = http_status.HTTP_401_UNAUTHORIZED
    headers = {'WWW-Authenticate': 'Bearer'}


async def get_current_user(x_uid: str = Header()) -> PersonModel:
    with SessionLocal.begin() as session:
        person = session.get(Person, x_uid)
        if not person:
            raise NotAuthenticated()
        return PersonModel.model_validate(person)
