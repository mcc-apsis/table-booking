import mimetypes

from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.gzip import GZipMiddleware
from fastapi.middleware.cors import CORSMiddleware

from .config import settings
from .middlewares import TimingMiddleware, ErrorHandlingMiddleware
from .db import engine
from .logging import get_logger
from .api import router as api_router

mimetypes.init()

logger = get_logger('nacsos.server')

app = FastAPI(openapi_url=settings.OPENAPI_FILE,
              openapi_prefix=settings.OPENAPI_PREFIX,
              root_path=settings.ROOT_PATH,
              separate_input_output_schemas=False)

logger.debug('Setting up server and middlewares')
mimetypes.add_type('application/javascript', '.js')
mimetypes.add_type('text/css', '.css')

app.add_middleware(ErrorHandlingMiddleware)

if settings.HEADER_CORS:
    app.add_middleware(CORSMiddleware,
                       allow_origins=settings.CORS_ORIGINS,
                       allow_methods=['GET', 'POST', 'DELETE', 'POST', 'PUT', 'OPTIONS'],
                       allow_headers=['*'],
                       allow_credentials=True)
    logger.info(f'CORSMiddleware will accept the following origins: {settings.CORS_ORIGINS}')
app.add_middleware(GZipMiddleware, minimum_size=1000)
app.add_middleware(TimingMiddleware)

logger.debug('Setup routers')
app.include_router(api_router, prefix='/api')

app.mount('/', StaticFiles(directory=settings.STATIC_FILES, html=True), name='static')

