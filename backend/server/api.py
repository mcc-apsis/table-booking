from datetime import date, datetime

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy import select, delete, or_

from .db import Base, engine, SessionLocal, PersonModel, Person, Booking, BookingModel, BookingType, GroupingModel, \
    Grouping
from .security import get_current_user

Base.metadata.create_all(bind=engine)
router = APIRouter()


@router.get('/people/', response_model=list[PersonModel])
def read_people(user: PersonModel = Depends(get_current_user)) -> list[PersonModel]:
    with SessionLocal.begin() as session:
        return [PersonModel.model_validate(p) for p in session.scalars(select(Person).order_by(Person.name))]


@router.post('/people/')
def create_update_person(person: PersonModel, user: PersonModel = Depends(get_current_user)) -> None:
    with SessionLocal.begin() as session:
        existing = session.get(Person, person.id)
        if existing:
            existing.name = person.name
            existing.short = person.short
            existing.acro = person.acro
            existing.group = person.group
            existing.type = person.type
        else:
            session.add(Person(**person.dict()))
        session.commit()


@router.delete('/people/')
def delete_person(uid: str, user: PersonModel = Depends(get_current_user)) -> None:
    with SessionLocal.begin() as session:
        session.execute(delete(Person).where(Person.id == uid))
        session.execute(delete(Booking).where(Booking.person == uid))
        session.commit()


@router.post('/booking/')
def create_booking(booking: BookingModel, user: PersonModel = Depends(get_current_user)) -> None:
    with SessionLocal.begin() as session:
        # Clear all potential conflicts (same person, desk, day) / ignoring meetings though
        session.execute(delete(Booking).where(Booking.day == booking.day,
                                              Booking.desk == booking.desk,
                                              Booking.person == booking.person,
                                              Booking.type != 'MEETING'))

        rslt = session.scalars(select(Booking).where(Booking.day == booking.day,
                                                     Booking.desk == booking.desk,
                                                     Booking.type == 'FULL_DAY')).all()

        if len(rslt) > 0:
            raise HTTPException(status_code=409, detail='No free booking for this desk and day.')

        session.add(Booking(**booking.dict()))
        session.commit()


@router.post('/bookings/')
def create_bookings(bookings: list[BookingModel], user: PersonModel = Depends(get_current_user)) -> None:
    with SessionLocal.begin() as session:
        for booking in bookings:
            session.add(Booking(**booking.dict()))
            session.flush()
        session.commit()


@router.post('/booking/edit')
def edit_booking(booking: BookingModel, user: PersonModel = Depends(get_current_user)) -> None:
    with SessionLocal.begin() as session:
        rslts: list[Booking] = []
        rslt: Booking = session.scalar(select(Booking).where(Booking.id == booking.id))
        if rslt:
            if rslt.series:
                rslts = session.scalars(select(Booking).where(Booking.series == booking.series)).all()
            else:
                rslts = [rslt]

        for rslt in rslts:
            if rslt.begin and rslt.end and rslt.begin >= rslt.end:
                raise ValueError('Invalid begin/end time')

            for col in Booking.__table__.columns:
                if col.name == 'id' or col.name == 'day':
                    pass
                elif col.name == 'begin' or col.name == 'end':
                    tm: datetime | None = getattr(rslt, col.name)
                    if tm is not None:
                        setattr(rslt,
                                col.name,
                                tm.replace(hour=getattr(booking, col.name).hour,
                                           minute=getattr(booking, col.name).minute))
                else:
                    setattr(rslt, col.name, getattr(booking, col.name))
            session.flush()
        session.commit()


@router.delete('/booking/')
def delete_booking(bid: str | None = None, series: str | None = None, after: date | None = None,
                   user: PersonModel = Depends(get_current_user)) -> None:
    if series is None and bid is None:
        raise AttributeError('Need at least series or bid.')

    with SessionLocal.begin() as session:
        if series is not None:
            stmt = delete(Booking).where(Booking.series == series, Booking.person == user.id)
            if after is not None:
                stmt = stmt.where(Booking.day >= after)
            session.execute(stmt)
        if bid is not None:
            session.execute(delete(Booking).where(Booking.id == bid, Booking.person == user.id))


@router.get('/bookings/')
def read_bookings(start: date, end: date, kind: BookingType | None = None,
                  user: PersonModel = Depends(get_current_user)) -> list[BookingModel]:
    with SessionLocal.begin() as session:
        stmt = select(Booking).where(Booking.day >= start, Booking.day <= end)
        if kind:
            stmt = stmt.where(Booking.type == kind)
        else:
            stmt = stmt.where(Booking.type != 'MEETING')
        rslt = session.scalars(stmt)
        return [BookingModel.model_validate(r) for r in rslt]


@router.get('/groupings/')
def read_groupings(user: PersonModel = Depends(get_current_user)) -> list[GroupingModel]:
    with SessionLocal.begin() as session:
        stmt = select(Grouping).where(or_(Grouping.owner == user.id, Grouping.public == True))
        rslt = session.scalars(stmt)
        return [GroupingModel.model_validate(r) for r in rslt]


@router.post('/grouping/')
def save_grouping(grouping: GroupingModel,
                  user: PersonModel = Depends(get_current_user)) -> None:
    with SessionLocal.begin() as session:
        rslt = session.scalar(select(Grouping).where(Grouping.id == grouping.id))
        if rslt:
            if rslt.owner == user.id:
                rslt.public = grouping.public
                rslt.name = grouping.name
                rslt.people = grouping.people
            else:
                raise HTTPException(status_code=403, detail='Not your group!')
        else:
            session.add(Grouping(**grouping.model_dump()))
        session.commit()


@router.delete('/grouping/')
def del_grouping(gid: str,
                 user: PersonModel = Depends(get_current_user)) -> None:
    with SessionLocal.begin() as session:
        session.execute(delete(Grouping)
                        .where(Grouping.id == gid,
                               Grouping.owner == user.id))
        session.commit()
