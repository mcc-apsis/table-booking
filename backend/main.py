#!/usr/bin/env python3

from server.logging import get_logger

logger = get_logger('tbs')
logger.info('Starting up server')

from server import app
