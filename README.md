# Table booking system
![screenshot.png](screenshot.png)

This is a simple table booking/reservation system for offices with (semi-)flex desks.
The assumption is, that by default most employees have "their" desk, which they can mark as "free" 
when they are not coming in, so that others can claim a workstation.

```
python -m venv venv
source venv/bin/activate
pip install -r backend/requirements.txt

cd frontend
npm install

# Set API endpoint (w/o trailing slash)
export VITE_TBS_BACKEND=https://domain.tld/api
export VITE_TBS_BASE=/tbs
npm run build

cd ../backend
# Adjust config in backend/config
# Run server
hypercorn --config=config/hypercorn.toml --reload main:app
```

Add this to a `/etc/sudoers.d/??` file:
```
# Allow GitLab Runner to start/stop table booking platform
gitlab-runner ALL= NOPASSWD: /bin/systemctl restart table-booking.service
gitlab-runner ALL= NOPASSWD: /bin/systemctl stop table-booking.service
gitlab-runner ALL= NOPASSWD: /bin/systemctl start table-booking.service
gitlab-runner ALL= NOPASSWD: /bin/systemctl status table-booking.service

gitlab-runner ALL= NOPASSWD: /usr/bin/chown -R gitlab-runner\:gitlab-runner /var/www/table-booking
gitlab-runner ALL= NOPASSWD: /usr/bin/chown -R tbs\:tbs /var/www/table-booking
```